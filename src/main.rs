use anyhow::Result;
use clap::{Parser, Subcommand};
use derive_more::Display;
use edit::edit_file;
use std::fs::OpenOptions;
use std::str::FromStr;

const LOGGFIL: &str = "logg.txt";

/// Loggning av arbetstid.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(subcommand)]
    action: Action,
}

#[derive(Subcommand, Debug)]
enum Action {
    /// Påbörja arbetstid.
    Start,
    /// Avsluta arbetstid.
    Stopp,
    /// Öppna loggfil i editor.
    Redigera,
    /// Visa loggfil.
    Visa,
    /// Visa status.
    Status,
    /// Granska
    Granska,
}

#[derive(Debug, strum_macros::EnumString, strum_macros::Display)]
#[strum(serialize_all = "lowercase")]
enum StateTransition {
    Start,
    Stopp,
}

#[derive(Debug, Display)]
#[display(fmt = "{}: {}", timestamp, state_transition)]
struct Event {
    timestamp: iso8601::DateTime,
    state_transition: StateTransition,
}

fn main() -> Result<()> {
    let args = Args::parse();
    match args.action {
        Action::Start => start(),
        Action::Stopp => stopp(),
        Action::Redigera => redigera(),
        Action::Visa => visa(),
        Action::Status => status(),
        Action::Granska => granska(),
    }
}

fn start() -> Result<()> {
    println!("God morgon!");
    logga("start")
}
fn stopp() -> Result<()> {
    println!("God afton!");
    logga("stopp")
}
fn redigera() -> Result<()> {
    println!("Läs och skriv!");
    edit_file(LOGGFIL).expect("Kunde inte öppna loggfilen.");
    Ok(())
}
fn visa() -> Result<()> {
    println!("Se, men inte röra!");
    use std::fs::read_to_string;
    use std::io::ErrorKind;

    print!(
        "{}",
        match read_to_string(LOGGFIL) {
            Ok(s) => s,
            Err(e) => match e.kind() {
                ErrorKind::NotFound => String::from(""),
                _ => panic!("{}", e),
            },
        }
    );

    Ok(())
}
fn status() -> Result<()> {
    println!("God dag!");
    Ok(())
}
fn granska() -> Result<()> {
    println!("Låt se!");

    use std::io::{self, BufRead};

    io::BufReader::new(
        OpenOptions::new()
            .read(true)
            .open(LOGGFIL)
            .unwrap_or_else(|x| panic!("Kunde inte öppna loggiflen \"{}\". {}", LOGGFIL, x)),
    )
    .lines()
    .enumerate()
    .map(|(n, l)| {
        (
            n,
            l.unwrap_or_else(|_| panic!("Kunde inte läsa rad {} i loggfilen.", n)),
        )
    })
    .map(|(n, l)| {
        let (timestamp, msg) = l
            .split_once(' ')
            .unwrap_or_else(|| panic!("Ogiltigt format på rad {}: {}", n, l));
        (
            iso8601::datetime(timestamp)
                .unwrap_or_else(|_| panic!("Ogiltig tidsstämpel: {}", timestamp)),
            StateTransition::from_str(msg)
                .unwrap_or_else(|_| panic!("Ogiltig händelse på rad {}: {}", n, msg)),
        )
    })
    .collect::<Vec<_>>()
    .into_iter()
    .for_each(|(timestamp, msg)| {
        println!("{}: {}", timestamp, msg);
    });

    Ok(())
}

fn logga(msg: &str) -> Result<()> {
    use std::io::Write;
    use time::format_description::well_known::Iso8601;

    let timestamp = time::OffsetDateTime::now_utc().format(&Iso8601::DEFAULT)?;

    OpenOptions::new()
        .create(true)
        .append(true)
        .open(LOGGFIL)
        .expect("Kunde inte öppna loggfilen.")
        .write_all(format!("{timestamp} {msg}\n").as_bytes())
        .expect("Kunde inte skriva till loggfilen.");

    Ok(())
}
